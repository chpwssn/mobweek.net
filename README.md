#This is the source code for the mobweek.net server
The code is separated into core functions, actions and displays.

core.php contains most of the functions and statistics calculations.

XML files display the formatting for the charts.

index.php contains the code for displaying the site.

admin/ is the administration portal

player/ is the player information page

rules/ display the rules of the game

tag/ handles the tagging logic

spot/ handles and records spots

