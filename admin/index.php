<!--<h1 style="align:center">Welcome To Mob Week</h1>-->
<?php
    //Copyright Chip Wasson Iceberg Technologies Limited
    //Mob Week.Net adminindex
    include '../core.php';
    connectDB();
    $unixtime = time();
	$gamestart = 1366725600;
    $currentplaytime = date_calc_diff($gamestart,$unixtime);
    //$action1 = "<a href='tag/?tag=";
    //$action2 = "'>tag</a>";
    $action1 = "<a href='./?verify=";
    $action2 = "'>verify</a>";
    $action3 = "<a href='./?reverse=";
    $action4 = "'>reverse</a>";
	$action5 = "<a href='./?disqualify=";
    $action6 = "'>disqualify</a>";
	$tag1 = "<a href='../tag/?admin=true&tag=";
    $tag2 = "'>tag</a>";
    //verification
    if(isset($_GET['verify'])){
        verify($_GET['verify']);
		echo "<meta http-equiv='refresh' content='0;url=http://mobweek.net/admin'>";
    }
    if(isset($_GET['reverse'])){
        reverse_tag($_GET['reverse']);
		echo "<meta http-equiv='refresh' content='0;url=http://mobweek.net/admin'>";

    }
	if(isset($_GET['disqualify'])){
		disqualify($_GET['disqualify']);	
	}
    
    
    //Top rankings
	echo "<div style='width:700px'>";
    //Rankings
	echo "<div style='float: left;width:30%'>";
    $teams = mysql_query("SELECT * FROM teams ORDER BY rank");
    echo "<i>Team Rankings:</i><br>";
    while($team = mysql_fetch_array($teams)){
        echo $team['rank'].". <b>Team ".$team['name']."</b> with <b>".$team['kills']."</b> kills.<br>";
    }
	echo "</div>";
	
	
	//Players Middle
	echo "<div style='float: left;width:30%'>";
    $players = mysql_query("SELECT * FROM players ORDER BY rank, name LIMIT 5");
    echo "<i>Player Rankings:</i><br>";
    while($player = mysql_fetch_array($players)){
        echo $player['rank'].". <b>".$player['name']."</b> with <b>".$player['kills']."</b> kills.<br>";
    }
	echo "</div>";
	
	//Teams Right
	echo "<div style='float: left;width:40%'>";
    $teams = mysql_query("SELECT * FROM teams ORDER BY lost, name");
    echo "<i>Teams with most alive:</i><br>";
	$place = 1;
    while($team = mysql_fetch_array($teams)){
        echo $place.". <b>Team ".$team['name']."</b> with <b>".($team['players']-$team['lost'])."</b> players left.<br>";
		$place++;
    }
	echo "</div>";
	
	echo "<br style='clear: left;' />";
	echo "</div>";
    
    $teams = mysql_query("SELECT name FROM teams ORDER BY name ");
    while($team = mysql_fetch_array($teams)){
        //Display Text
        echo "<h2>Team ".$team['name']."</h2>";
        echo "<table border=1><tbody style='text-align:center'><!-- Results table headers --><tr><th>Player</th><th>Time In Play</th><th>Status</th><th>Kills</th><th>Rank</th><th>Out By</th><th>Action</th></tr>";
        //Begin Displaying Players
        $teamA = get_players($team['name']);
        while($player = mysql_fetch_array($teamA)){
            //Status box formating and outby
            if($player['status'] == "in"){
                $statuscolor = "green";
                $outby = "";
            }else if ($player['status'] == "tagged"){
                $statuscolor = "yellow";
                $outby = $player['outby'];
            }else if ($player['status'] == "out"){
                $statuscolor = "red";
                $outby = $player['outby'];
            }else{
                $statuscolor = "orange";
                $outby = "";
            }
            //Time play calc
            if($player['deadtime'] == 0)
                $playtime = $currentplaytime;
            else
                $playtime = date_calc_diff($gamestart,$player['deadtime']);
            echo "<tr><td><a style='text-decoration:none' href='player/?id=".$player['id']."'>".$player['name']."</a></td><td>".$playtime."</td><td bgcolor='$statuscolor'>".$player['status']."</td><td>".$player['kills']."</td><td>".$player['rank']."</td><td>".$outby."</td><td>".$tag1.$player['id'].$tag2." ".$action1.$player['id'].$action2." ".$action3.$player['id'].$action4." ".$action5.$player['id'].$action6."</tr>";
        }
        echo "</tbody></table>";
    }
    
    ?>
