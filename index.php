<title>CSU MobWeek</title>
<link rel="icon" href="http://mobweek.net/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="http://www.example.com/favicon.ico" type="image/vnd.microsoft.icon">
<?php
  //Copyright Chip Wasson Iceberg Technologies Limited
  //Mob Week.Net index
    include 'core.php';
    connectDB();
    $unixtime = 1367085600;
	$gamestart = 1366725600;
    $currentplaytime = date_calc_diff($gamestart,$unixtime);
    $action1 = "<a href='tag/?tag=";
    $action2 = "'>tag</a>";
    //$action1 = "<a href='./?verify=";
    //$action2 = "'>verify</a>";
	
    //Top rankings
	echo "<div style='width:700px'>";
    //Rankings
	echo "<div style='float: left;width:30%'>";
    $teams = mysql_query("SELECT * FROM teams ORDER BY rank");
    echo "<i>Team Rankings:</i><br>";
    while($team = mysql_fetch_array($teams)){
        echo $team['rank'].". <b>Team ".$team['name']."</b> with <b>".$team['kills']."</b> kills.<br>";
    }
	echo "</div>";
	
	
	//Players Middle
	echo "<div style='float: left;width:30%'>";
    $players = mysql_query("SELECT * FROM players ORDER BY rank, name LIMIT 5");
    echo "<i>Player Rankings:</i><br>";
    while($player = mysql_fetch_array($players)){
        echo $player['rank'].". <b>".$player['name']."</b> with <b>".$player['kills']."</b> kills.<br>";
    }
	echo "</div>";
	
	//Teams Right
	echo "<div style='float: left;width:40%'>";
    $teams = mysql_query("SELECT * FROM teams ORDER BY lost, name");
    echo "<i>Teams with most alive:</i><br>";
	$place = 1;
    while($team = mysql_fetch_array($teams)){
        echo $place.". <b>Team ".$team['name']."</b> with <b>".($team['players']-$team['lost'])."</b> players left.<br>";
		$place++;
    }
	echo "</div>";
	
	echo "<br style='clear: left;' />";
	echo "</div>";
    
	//In game or not
	if($currentplaytime <= 0)
		echo "<h4><i>The game has not yet begun. It begins in ".date_calc_diff($unixtime,$gamestart).", be sure to check the <a style='text-decoration:none' href='rules'>rules</a>.</i></h4>";
    if(!inplay()){
        echo "<h3 style='color:red'>The game is off for the day.</h3>";
    } else {
		echo "<p>Once you hit someone with a sock: <ol><li>Click 'tag' for their name.</li> <li>Find your name in the list.</li> <li>Click 'Tag'.</li><li>Their status is now 'tagged'</li> </ol>Once Tripp verifies it, they will be officially 'out'.</p>";
        //Spot Map
        echo "<div style='width:800px'>";
        //Map area
        echo "<div style='float: left;width:65%'>";
        include 'map.php';
        echo "</div>";
        echo "<div style='float: left;width:35%'>";
        $spots = mysql_query("SELECT * FROM spots ORDER BY time DESC LIMIT 7");
        echo "Recent spots:";
        echo "<ul>";
        while($spot = mysql_fetch_array($spots)){
            echo "<li><i>".$spot['name']."</i> spotted in area ".$spot['area']."<br>".$spot['timestring'].".";
            if(!$spot['note'] == "")
                echo " ".$spot['note'];
            echo "</li>";
        }
        echo "<li> <a href='spot/?all' style='text-decoration:none'>See all spots...</a></li>";
        echo "</ul>";
        echo "</div>";
        echo "<br style='clear: left;' />";
        echo "</div>";
    }
	
    $teams = mysql_query("SELECT name FROM teams ORDER BY name ");
    while($team = mysql_fetch_array($teams)){
        //Display Text
        echo "<h2>Team ".$team['name']."</h2>";
        echo "<table border=1><tbody style='text-align:center'><!-- Results table headers --><tr><th>Player</th><th>Time In Play</th><th> Status </th><th> Kills </th><th> KPD </th><th> Rank </th><th> Out By </th><th>Action</th></tr>";
        //Begin Displaying Players
        $teamA = get_players($team['name']);
        while($player = mysql_fetch_array($teamA)){
            //Status box formating and outby
            if($player['status'] == "in"){
                $statuscolor = "green";
				$actionstring = $action1.$player['id'].$action2;
                $outby = "";
            }else if ($player['status'] == "tagged"){
                $statuscolor = "yellow";
                $outby = $player['outby'];
				$actionstring = "";
            }else if ($player['status'] == "out"){
                $statuscolor = "red";
                $outby = $player['outby'];
				$actionstring = "";
            }else{
                $statuscolor = "orange";
				$actionstring = "";
                $outby = "";
            }
            //added to archive!
            $actionstring="";
            //Time play calc
            if($player['deadtime'] == 0)
                $playtime = $currentplaytime;
            else
                $playtime = date_calc_diff($gamestart,$player['deadtime']);
            echo "<tr><td><a style='text-decoration:none' href='player/?id=".$player['id']."'>".$player['name']."</a></td><td>".$playtime."</td><td bgcolor='$statuscolor'>".$player['status']."</td><td>".$player['kills']."</td><td>".round(($player['kills']/days_in_game()),2)."</td><td>".$player['rank']."</td><td>".$outby."</td><td>".$actionstring."</tr>";
        }
        echo "</tbody></table>";
    }
    echo "<br><br><h3>Game \"awards\":</h3>";
	$firstkill = checkFirstKill();
	echo "<h4>First blood:</h4>";
	if($firstkill == null){
		echo "The first kill has not been made yet.";
	} else {
		echo $firstkill['name']." killed by ".$firstkill['outby'];
	}
    
	echo "<h3>Recent Notes:</h3>";
	$notes = mysql_query("SELECT * FROM notes ORDER BY id DESC LIMIT 10");
	while($note = mysql_fetch_array($notes)){
		echo "<a href='player/?id=".$note['pid']."'>".$note['note']."</a> on ".get_player_name($note['pid'])."<br>";
	}
    echo "<br>";
    include 'graph.php';
	echo "<meta http-equiv='refresh' content='360;url=http://mobweek.net'>";

    ?>
<!--<table border=1>
<tbody>-->
<!-- Results table headers -->
<!--<tr>
<th>Player</th>
<th>Time In Play</th>
<th>Status</th>
<th>Kills</th>
<th>Rank</th>
<th>Action</th>
</tr>
<tr>
<td>name</td>
<td>timecalc</td>
<td>status</td>
<td>kills</td>
<td>rankcalc</td>
<td>actions</td>
</tr>
</tbody>
</table>-->