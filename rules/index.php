<title>CSU MobWeek Rules</title>
<h2>CSU MobWeek Rules</h2>
<a href="../" style="text-decoration:none">&larr;back</a>
<ol>
	<li>Members can only be tagged out within the square mile of campus. (Laurel to Prospect and Shields to College)</li>
    <li>Dorms are fair game.</li>
    <li>Members can only be tagged out between 8 a.m. and 5:30 p.m.</li>
    <li>Within arm reach of one's formal date is considered a safe zone.</li>
    <li>Playing intramural sports, practicing for a club sport, working at a paying job on campus and ROTC are considered safe zones.</li>
    <li>You may only use two pairs of socks to tag people out.</li>
    <li>You may block a sock with your book bag/folder/etc. as long as it is an obvious block.
    	<ul><li>Catching a sock is considered a block</li></ul>
        </li>
    <li>Members who are still in the game must <b>Clearly</b> display their bandanas at all times. Those members still in who are caught without their bandana or who are hiding it in any way are out.</li>
    <li>When a member is tagged out he must give up his bandana (they can be reclaimed at the end of the week).</li>
    <li>Once tagged out, a member cannot tag anyone else out.</li>
    <li>When a member is tagged out, his bandana must be presented to the V.P. of Programming (Tripp). That member will then be crossed off the team list and notified that he is officially out.</li>
    <li>All official rulings on who is in and who is out will be posted on the front door ever night and sent out in an email on Wednesday and Friday.</li>
    <li>Use common sense and <b>be respectful</b> of people's classes and tests (even if they are still fair game).</li>
    <li>Official game time is from 8:00 a.m. Tuesday until noon on Saturday.</li>
    <li>Team with the most survivors at noon on Saturday wins.</li>
</ol>