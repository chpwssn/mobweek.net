<?php
    //Copyright Chip Wasson Iceberg Technologies Limited
    //Mob Week.Net playerindex
    include '../core.php';
    connectDB();
    if(isset($_POST['note'])){
        add_new_note($_POST['note'],$_POST['id'],date("F j, Y, g:i a"));
        echo "<meta http-equiv='refresh' content='0;url=http://mobweek.net/player/?id=".$_POST['id']."'>";
    }
    if(isset($_GET['id'])){
        echo "<a style='text-decoration:none' href='http://mobweek.net'>&larr;back</a><br>";
        $player = get_player_info($_GET['id']);
        echo "<h1>Player Name: ".$player['name']."</h1><br>";
        echo "Member Of: ".$player['team']."<br>";
        echo "Status: ".$player['status']."<br>";
        if($player['status'] == "out" || $player['status'] == "tagged")
            echo "Out By: ".$player['outby']."<br>";
        echo "Kills: ".$player['kills']."<br>";
        echo "Rank: ".$player['rank']."<br>";
        echo "<h1>Notes:</h1><br>";
        $notes = mysql_query("SELECT * FROM notes WHERE pid='".$_GET['id']."'");
        $notecount=0;
        while($note = mysql_fetch_array($notes)){
            echo $note['note']." <b>@ ".$note['time']."</b><br><br>";
            $notecount++;
        }
        if(!$notecount)
            echo "<i>No notes added for this player yet.</i><br><br>";
        echo "Add Note:<br>";
        echo "<form name='myform' action='./' method='POST'><textarea cols='40' rows='5' name='note'>Class times, known locations, etc.</textarea><input type='hidden' name='id' value='".$_GET['id']."'><br><input type='submit' value='Post Note'></form>";
    }else
    echo "<meta http-equiv='refresh' content='0;url=http://mobweek.net'>";
?>